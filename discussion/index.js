console.log("Hello");


/*
	OBJECTS

	-an object is a data type that is used to represent real world objects
	-it is a collection of related data and / or functionalities
	-information stored in objects are represented in "key:value" pair
	-different data types may be stored in an object

	CREATING OBJECTS:
		-object initializers or literal notation
		-constructor functions
	
	CRETING OBJECTS through object initializers or literal notation

	-this creates or declares an object and also initializes or assigns its values upon creation
	-it already has its keys and value such as name, color, weight, etc.

	SYNTAX:
		let/const objectName = {
			KeyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: "Nokia 3210",
	namufactureDate: 1999
};
console.log("Result from creating objects using initializers or literal notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log("");

/*
	creating objects using construstor functions

	-creates a reusable function to create several objects that have the same data structure
	-useful for creating multiple instances or copies of an object
	-an instace is a concrete occurence of any object which emphasizes on the distinct or unique of it

	SYNTAX:
		function objectName(keyA, keyB){
			this.keyA = keyA,
			this.keyB = keyB
		}
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

function Student(firstName, lastName, yearOld, city){
	this.name = firstName + " " + lastName;
	this.age = yearOld;
	this.residence = city;
}

/*
	The THIS keyword allows us to assign a new object's properties by associating them with values receiving from a constructor function
*/

// this is an instance of the Laptop Object
console.log("this is a unique instace of the Laptop Object using the constructor")
let laptop_mica = new Laptop("Lenovo", 2008)
console.log(laptop_mica);
let laptop_eric = new Laptop("HP", 1990);
console.log(laptop_eric);

console.log("this is a unique instance of the student object")
let elaine = new Student("Elaine", "SJ", 12, "Mars");
console.log(elaine);

let jake = new Student("Jake", "Lex", 10, "Sun");
console.log(jake);

/*
	-The NEW keyword creates an instace of an object
*/

/*
	ACCESSING THE OBJECT PROPERTIES

	Using the dot notation
*/

console.log(laptop_mica.name);
console.log(elaine.name);

// using the square bracket notation

console.log("Result from square bracket notation: "+laptop_mica["name"]);
console.log("Result from square bracket notation: "+elaine["name"]);

// Initializing , adding, delete or reassigning object properties

/*
	-like any other variable in javascript, objects may have their own properties initialized after the object was created or declared
	-this is useful for times when an object's properties are undetermined at the time of beginning.
*/

// EMPTY OBJECT
console.log("Empty car object:");
let car = {};
console.log(car);
console.log("");

// initializing or adding object properties using dot notation
car.name = "Honda Civic"
console.log("Result from adding properties using dot notation:")
console.log(car);

// initializing or adding object properties using bracket notation
car["Manufacture Date"] = 2019;
console.log(car["Manufacture Date"]);

// deleting object properties
delete car["Manufacture Date"];
console.log("result from deleting properties");
console.log(car);

// reassign object properties
car.name = "Dodge Carger R/T";
console.log("Result from reassigning properties");
console.log(car);
console.log("");

// OBJECT METHODS

/*
	-a method is a function
	-attached to an object as a property
	-useful for creating object specific properties
	-similar of functions of real worlds object, methods are defined based on what an object is capable of doing.
*/

let person = {
	name: "John",
	talk: function(){
		console.log("Hello, my name is "+this.name)
	}
}
console.log(person);
console.log("Result from object methods");
person.talk();


person.walk = function(){
	console.log(this.name+" walked 25 steps forward.");
}
person.walk();

// Methods are usuful for creating reusable functions that perform tasks related to objects

let friend = {
	firstName: "Arjay",
	lastName: "Dala",
	address: {
		city: "Etibak",
		country: "New York"
	},
	emails: ["arjay@friendster.com", "arjay@myspace.com"],
	introduce: function(classmate){
		console.log("Hello, my name is "+this.firstName+" "+this.lastName+". Nice to meet you "+classmate)
	}
}
friend.introduce("Erick");
friend.introduce("Mica");


let mypokemon = {
	name: "Pickachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(targetPokemon){
		console.log(this.name+" tackled "+targetPokemon);
		console.log(targetPokemon + "'s health is  reduced to 3 "+  targetPokemon + "'s health");
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}
console.log(mypokemon);
mypokemon.tackle("charmander");
mypokemon.tackle("Arceus");
console.log("");

// create pokemon character using constructor function
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name+" tackled "+target.name)
		console.log(target.name +"'s health is now reduced to "+target.health)
	};
	this.faint = function(){
		console.log(this.name+" fainted");
	}
}

let pickachu = new Pokemon("Pickachu", 16);
let ratata = new Pokemon("Ratata", 8);
pickachu.tackle(ratata);
console.log("");



function Hunter (name, level, defense, speed, nenpower){
	this.name = name;
	this.level = name;
	this.name = name;
	this.name = name;
	this.name = name;
	this.name = name;
	this.name = name;
	this.name = name;
}



// Product
function shoppeeProduct(name, initialPrice, discount){
	this.name = name;
	this.price = initialPrice+500;
	this.discount = function discount(value){
		let discountedPrice = this.price-value;
		console.log("The total discounted price is "+discountedPrice)
	};
	this.stock = function(quantity){
		if(quantity <= 5){
			alert("babagsak na ang negosyo");
		}else{
			alert("goods goods pa")
		}
	}
}

let iPhoneX = new shoppeeProduct("iPhoneX", 3000, 0);
iPhoneX.discount(100);
iPhoneX.stock(2);